queue = {}
function queue.new()
	return {
		front_idx = 1,
		tail_idx = 1
	}
end

function queue.push(q, item)
	q[q.front_idx] = item
	q.front_idx = q.front_idx + 1
end

function queue.pop(q)
	item = q[q.tail_idx]
	q[q.tail_idx] = nil
	q.tail_idx = q.tail_idx + 1
	return item
end

function queue.front(q)
	return q[q.front_idx-1]
end

function queue.size(q)
	return q.front_idx-q.tail_idx
end

function create_maze(size)
	
	local _xy = {0,2,0,-2,0}
	-- local size = 50
	local base = size+10
	local road = {}

	points = {}
	function dfs(curr_x, curr_y)

		road[curr_x*base+curr_y] = true
		if math.random(1,10) <= 3 then
			points[curr_x*base+curr_y] = true
			return
		end

		local permutation = {1,2,3,4}
		
		for i=1,4 do
			local l = math.random(1,4)
			local r = math.random(1,4)
			permutation[l], permutation[r] = permutation[r], permutation[l]
		end
		
		for i=1, 4 do
			local next_x = curr_x+_xy[permutation[i]]
			local next_y = curr_y+_xy[permutation[i]+1]
			if  next_x>=1 and next_x<=size and
				next_y>=1 and next_y<=size and
				road[next_x*base+next_y] == nil
			then
				local mid_x = math.floor((curr_x+next_x)/2)
				local mid_y = math.floor((curr_y+next_y)/2)
				road[mid_x*base+mid_y] = true
				dfs(next_x, next_y)
			end
		end
	end

	points[1*base+1] = true

	repeat
		local has_point = false
		for v,_ in pairs(points) do
			has_point = true
			points[v] = nil
			dfs(math.floor(v/base), v%base)
			break
		end
		-- print(has_point)
	until not has_point


	return road
end

math.randomseed(os.time())
local game_size = 71
local base = game_size+10
local line_width = 3
local cell_size = ((700 - line_width)/(math.floor(game_size/2)+1)-line_width)
local maze = create_maze(game_size)


function love.load()
	local wh = 700
	-- local wh = (game_size+2)*cell_size
	love.window.setMode(wh, wh)
	love.graphics.setBackgroundColor(1,1,1,1)
end

local player = {{1,1}, {game_size, game_size}}

local _xy = {0,1,0,-1,0}
local key_to_dir = {
	["down"] = 1,
	["right"] = 2,
	["up"] = 3,
	["left"] = 4,
	["s"] = 5,
	["d"] = 6,
	["w"] = 7,
	["a"] = 8
}

function new_player(pos, keys, color, trail_pos)
	-- trail_pos = trail_pos or {}
	-- pos: {x, y}
	-- keys: {down, right, up, left}
	-- color: {r, g, b, a}
	local player = {
		["pos"] = pos,
		["keys"] = keys,
		["color"] = color,
		["trail_pos"] = trail_pos,
		["move"] = function (key)
			for idx=1, 4 do
				if key==keys[idx] then
					print(pos[1], pos[2])
					next_x = pos[1]+_xy[idx]
					next_y = pos[2]+_xy[idx+1]
					if maze[next_x*base+next_y]~=nil then
						trail_pos[pos[1]*base+pos[2]] = true
						trail_pos[next_x*base+next_y] = true

						print('true', pos[1], pos[2])
						pos[1], pos[2] = pos[1]+_xy[idx]*2, pos[2]+_xy[idx+1]*2
					end
				end
			end
		end
	}
	return player
end

local players = {
	new_player({game_size, game_size}, {"down", "right", "up", "left"},
		{["r"]=30/255, ["g"]=144/255, ["b"]=255/255, ["a"]=1}, {}),
	new_player({1, 1}, {"s", "d", "w", "a"},
		{["r"]=255/255, ["g"]=20/255, ["b"]=147/255, ["a"]=1}, {}),
	new_player({1, game_size}, {"k", "l", "i", "j"},
		{["r"]=255/255, ["g"]=0/255, ["b"]=255/255, ["a"]=1}, {})
}
local game_win = false

function love.keypressed(key)
	for _, player in pairs(players) do
		player.move(key)
	end
	if math.abs(players[1].pos[1]-players[2].pos[1]) +
	   math.abs(players[1].pos[2]-players[2].pos[2])
	   <= 1
	   then
		game_win = true
	end
end

function love.draw()
	

	if game_win then
		return
	end
	local pos_x = 0
	local pos_y = 0
	for x=0, game_size+1 do
		local width = x%2==0 and line_width or cell_size
		pos_y = 0
		for y=0, game_size+1 do
			local height = y%2==0 and line_width or cell_size


			if maze[x*base+y] == true then
				local drawed = false
				for _, player in pairs(players) do
					if player.trail_pos[x*base+y]~=nil then
						drawed = true
						love.graphics.setColor(player.color.r, 0.5, player.color.b, player.color.a)
						-- love.graphics.rectangle("fill", x*cell_size, y*cell_size, cell_size, cell_size);
						love.graphics.rectangle("fill", pos_x, pos_y, width, height);
					end
				end
				if not drawed then
					love.graphics.setColor(255/255, 250/255, 250/255, 1)
					-- love.graphics.rectangle("fill", x*cell_size, y*cell_size, cell_size, cell_size);
					love.graphics.rectangle("fill", pos_x, pos_y, width, height);
				end
			else
				love.graphics.setColor(128/255, 128/255, 128/255, 1)
				love.graphics.rectangle("fill", pos_x, pos_y, width, height);
			end

			for _, player in pairs(players) do
				local x = player.pos[1]
				local y = player.pos[2]
				local width = x%2==0 and line_width or cell_size
				local height = y%2==0 and line_width or cell_size
				local pos_x = math.floor(x/2)*cell_size+(x-math.floor(x/2))*line_width
				local pos_y = math.floor(y/2)*cell_size+(y-math.floor(y/2))*line_width


				love.graphics.setColor(player.color.r, player.color.g, player.color.b, player.color.a)
				-- love.graphics.rectangle("fill", player.pos[1]*cell_size, player.pos[2]*cell_size, cell_size, cell_size);
				love.graphics.rectangle("fill", pos_x, pos_y, width, height);
				-- for i=player.trail_pos.tail_idx, player.trail_pos.front_idx-1 do
				-- -- for _, pos in pairs(player.trail_pos) do
				-- 	if player.trail_pos[i]==nil then break end
				-- 	print(i, pos)
				-- end
			end

			-- love.graphics.setColor(255/255, 20/255, 147/255, 1)
			-- love.graphics.rectangle("fill", player2.pos[1]*cell_size, player2.pos[2]*cell_size, cell_size, cell_size);
			pos_y = pos_y+height
		end
		pos_x = pos_x+width
	end

end
